package com.example;

import com.example.annotations.LogMe;
import com.example.annotations.LogOnError;
import com.example.annotations.TimeMe;

public interface FooService {

    @LogOnError
    void method1();

    @LogMe
    @TimeMe
    void method2(String string,
                 int i,
                 boolean b);
}
