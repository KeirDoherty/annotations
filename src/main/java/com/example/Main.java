package com.example;

public class Main {

    /*
     * Implement the annotations
     */
    public static void main(String[] args) {

        FooService fooService = new FooServiceImpl();

        try {
            fooService.method1();
            throw new RuntimeException("Should have been an exception thrown by method1.");
        } catch (RuntimeException ignored) {
        }

        fooService.method2("some string", 250, false);
    }

}

