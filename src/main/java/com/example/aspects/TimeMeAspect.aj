package com.example.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class TimeMeAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeMeAspect.class.getSimpleName());

    @Around("@annotation(com.example.annotations.TimeMe) && execution(* *(..))")
    public Object timeMe(ProceedingJoinPoint joinPoint) throws Throwable {

        String name = joinPoint.getSignature().getName();

        Object object = null;
        long start = System.currentTimeMillis();

        try {
            object = joinPoint.proceed();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            LOGGER.info(name + " duration: " + (System.currentTimeMillis() - start) + "ms");
        }

        return object;
    }
}