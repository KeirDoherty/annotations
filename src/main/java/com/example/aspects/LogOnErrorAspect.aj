package com.example.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class LogOnErrorAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogOnErrorAspect.class.getSimpleName());

    @Around("@annotation(com.example.annotations.LogOnError) && execution(* *(..))")
    public Object logOnError(ProceedingJoinPoint joinPoint) throws Throwable {

        Object object = null;

        try {
            object = joinPoint.proceed();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return object;
    }
}