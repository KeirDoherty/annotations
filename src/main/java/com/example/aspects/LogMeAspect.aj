package com.example.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

@Aspect
public class LogMeAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogMeAspect.class.getSimpleName());

    @Around("@annotation(com.example.annotations.LogMe) && execution(* *(..))")
    public Object logMe(ProceedingJoinPoint joinPoint) throws Throwable {
        LOGGER.info("calling: " + joinPoint.getSignature().getName() + " with parameters: " + Arrays.toString(joinPoint.getArgs()));
        return joinPoint.proceed(joinPoint.getArgs());
    }
}