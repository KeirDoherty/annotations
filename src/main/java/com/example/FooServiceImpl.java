package com.example;

import com.example.annotations.LogMe;
import com.example.annotations.LogOnError;
import com.example.annotations.TimeMe;

public class FooServiceImpl implements FooService {

    @LogOnError
    @Override
    public void method1() {
//        System.out.println("Explicit sout no failure test");
        throw new RuntimeException("Always fails");
    }

    @LogMe
    @TimeMe
    @Override
    public void method2(String string, int i, boolean b) {
        try {
            // println when the method is executing to ensure it occurs once per call, not once per annotation.
            System.out.println("method2 is executing now");
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
